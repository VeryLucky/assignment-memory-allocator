#pragma once

#include <string.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

enum test_status {
        TEST_OK = 0,
        TEST_ERROR
};

static const char* const test_msg[] = {
        [TEST_OK] = " was successful\n",
        [TEST_ERROR] = " wasn't successful\n"
};



enum test_status testing(FILE* test_status);

