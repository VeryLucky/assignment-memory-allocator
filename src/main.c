#include "tests.h"

int main() {
    FILE *f1 = fopen("tests_result.txt", "wb");
    int status = 0;
    if (f1 == NULL) printf("H");
    else {
        status = testing(f1);
        if (status == 0) printf("All tests passed");
        else printf("Not all tests passed (look tests_result.txt)");
        fclose(f1);
}
    return status;
}
