#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) {
    if (query < BLOCK_MIN_CAPACITY) return block->capacity.bytes >= BLOCK_MIN_CAPACITY;
    else return block->capacity.bytes >= query;
}
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {

    struct region region;
    bool extends = true;
    const size_t region_size = region_actual_size(query);

    void* mmap_status = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (mmap_status == MAP_FAILED) {
        mmap_status = map_pages(addr, region_size, 0);
        extends = false;
        printf("no");
    }
    if (mmap_status == MAP_FAILED) return REGION_INVALID;
    region = (struct region) {mmap_status, query, extends};
    block_size bs = {region_size};
    block_init(mmap_status, bs, NULL);
    return region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && size_max(query, BLOCK_MIN_CAPACITY) + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block_splittable(block, query)) {
        size_t first_block_capacity = query;
        if (query < BLOCK_MIN_CAPACITY) first_block_capacity = BLOCK_MIN_CAPACITY;
        block_size first_block_size = size_from_capacity((block_capacity){first_block_capacity});
        block_size second_block_size = size_from_capacity((block_capacity){block->capacity.bytes - first_block_capacity - offsetof(struct block_header, contents)});
        block_init((uint8_t*)block + first_block_size.bytes, second_block_size, NULL);
        block->next = (struct block_header*)((uint8_t*)block + first_block_size.bytes);
        block->capacity.bytes = first_block_capacity;
        return true;
    } else return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (block->next != NULL && mergeable(block, block->next)) {
        block_capacity new_capacity = (block_capacity){block->capacity.bytes + block->next->capacity.bytes + offsetof(struct block_header, contents)};
        block_init(block, size_from_capacity(new_capacity), block->next->next);
        return true;
    } else {
        return false;
    }
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {

    bool status = false;
    struct block_header* found_block = block;
    while (!status) {
        if (found_block->is_free) {
            while(try_merge_with_next(found_block));
            if (block_is_big_enough(sz, found_block)) {
                status = true;
            } else {
                if (found_block->next != NULL) found_block = found_block->next;
                else return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, found_block};
            }
        } else {
            if (found_block->next != NULL) found_block = found_block->next;
            else return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, found_block};

        }
    }
    return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, found_block};


}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result bsr;

    if (block->capacity.bytes >= BLOCK_MIN_CAPACITY) {
        bsr = find_good_or_last(block, query);
        if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
            split_if_too_big(bsr.block, query);
        }
    } else return (struct block_search_result){BSR_CORRUPTED, block};
    return bsr;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {

    void* new_addr = (uint8_t*)last + size_from_capacity(last->capacity).bytes;

    struct region new_region = alloc_region(new_addr, size_from_capacity((block_capacity){query}).bytes);
    last->next = new_region.addr;

    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

    struct block_search_result bsr = try_memalloc_existing(query, heap_start);

    if (bsr.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header *bh = heap_start;
        while (bh->next != NULL) {bh = bh->next;}
        grow_heap(bh, query);
        bsr = try_memalloc_existing(query, heap_start);
    } else if (bsr.type == BSR_CORRUPTED) {
        bsr.block = heap_init(query);
        bsr = try_memalloc_existing(query, bsr.block);
    }
    bsr.block->is_free = false;
    return bsr.block;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
