#include <malloc.h>
#include "tests.h"

static enum test_status allocated_memory_test(FILE* result) {

    fprintf( result, "%s","Test 1: \n");
    enum test_status status;
    int32_t *var = _malloc(sizeof(int32_t));
    struct block_header* bh = (struct block_header *) ((uint8_t *) var - offsetof(struct block_header, contents));
    if (bh->capacity.bytes == size_max(sizeof(*var), BLOCK_MIN_CAPACITY) && !bh->is_free) {
        if (bh->next != NULL && bh->next->is_free && bh->next->next == NULL) {
            status = TEST_OK;
        } else status = TEST_ERROR;
    } else status = TEST_ERROR;

    fprintf(result, "%s","Heap after memory allocation: \n");
    debug_heap(result, bh);
    fprintf(result,"%s%s" , "Test: allocation memory", test_msg[status]);
    fprintf(result, "%s","\n");
    _free(var);
    return status;
}

static enum test_status free_one_block_test(FILE* result) {

    fprintf( result, "%s","Test 2: \n");
    enum test_status status;
    int32_t *var1 = _malloc(sizeof(int32_t));
    int32_t *var2 = _malloc(sizeof(int32_t));
    int32_t *var3 = _malloc(sizeof(int32_t));
    *var2 = 10;
    struct block_header* bh1 = (struct block_header *) ((uint8_t *) var1 - offsetof(struct block_header, contents));
    fprintf(result, "%s","Heap before memory releasing: \n");
    debug_heap(result, bh1);
    _free(var2);
    struct block_header* bh2 = bh1->next;
    struct block_header* bh3 = NULL;
    if (bh2 != NULL) {
        bh3 = bh2->next;
        if (bh3 != NULL && !bh1->is_free && bh2->is_free && !bh3->is_free && bh3->next != NULL) status = TEST_OK;
        else status = TEST_ERROR;
    } else status = TEST_ERROR;
    fprintf(result, "%s","Heap after memory releasing: \n");
    debug_heap(result, bh1);
    fprintf(result,"%s%s" , "Test: free one block", test_msg[status]);
    fprintf(result, "%s","\n");
    _free(var3);
    _free(var1);
    return status;
}

static enum test_status free_two_blocks_test(FILE* result) {

    fprintf( result, "%s","Test 3: \n");
    enum test_status status;
    int32_t *var1 = _malloc(sizeof(int32_t));
    int32_t *var2 = _malloc(sizeof(int32_t));
    int32_t *var3 = _malloc(sizeof(int32_t));
    *var2 = 10;
    struct block_header* bh = (struct block_header *) ((uint8_t *) var1 - offsetof(struct block_header, contents));
    fprintf(result, "%s","Heap before memory releasing: \n");
    debug_heap(result, bh);
    _free(var3);
    _free(var2);
    if (bh->capacity.bytes == size_max(sizeof(*var1), BLOCK_MIN_CAPACITY) && !bh->is_free) {
        if (bh->next != NULL && bh->next->is_free && bh->next->next == NULL) {
            status = TEST_OK;
        } else status = TEST_ERROR;
    } else status = TEST_ERROR;
    fprintf(result, "%s","Heap after memory releasing: \n");
    debug_heap(result, bh);
    fprintf(result,"%s%s" , "Test: free two blocks", test_msg[status]);
    fprintf(result, "%s","\n");
    _free(var3);
    _free(var1);
    return status;
}

static enum test_status grow_heap_with_extending_test(FILE* result) {

    fprintf( result, "%s","Test 4: \n");
    enum test_status status;
    int32_t *var1 = _malloc(sizeof(int32_t));
    struct block_header* bh1 = (struct block_header *) ((uint8_t *) var1 - offsetof(struct block_header, contents));
    size_t old_capacity = bh1->capacity.bytes + bh1->next->capacity.bytes;
    int32_t *var2 = _malloc(bh1->next->capacity.bytes + 2000);
    struct block_header* bh2 = bh1->next;
    if (bh2 != NULL) {
        struct block_header *bh3 = bh2->next;
        *var2 = 1;
        if (bh3 != NULL && bh3->next == NULL) {
            size_t new_capacity = bh1->capacity.bytes + bh2->capacity.bytes + bh3->capacity.bytes;
            if (new_capacity > old_capacity) status = TEST_OK;
            else status = TEST_ERROR;
        } else status = TEST_ERROR;
    } else status = TEST_ERROR;
    fprintf(result, "%s","Heap after growing: \n");
    debug_heap(result, bh1);
    _free(var2);
    _free(var1);
    if (bh1->next != NULL) status = TEST_ERROR;
    fprintf(result, "%s","Heap after releasing: \n");
    debug_heap(result, bh1);
    fprintf(result,"%s%s" , "Test: growing heap with extending", test_msg[status]);
    fprintf(result, "%s","\n");
    return status;
}

static enum test_status grow_heap_without_extending_test(FILE* result) {

    fprintf( result, "%s","Test 5: \n");
    enum test_status status;
    int32_t *var1 = _malloc(100);
    uint8_t *var3 = _malloc(5000);
    struct block_header* bh1 = (struct block_header *) ((uint8_t *) var1 - offsetof(struct block_header, contents));
    bh1->next = bh1->next->next;
    _free(var3);
    size_t old_capacity = bh1->capacity.bytes + bh1->next->capacity.bytes;
    int32_t *var2 = _malloc(bh1->next->capacity.bytes + 2000);
    struct block_header* bh2 = bh1->next;
    if (bh2 != NULL) {
        struct block_header *bh3 = bh2->next;
        *var2 = 1;
        if (bh3 != NULL && bh3->next == NULL) {
            size_t new_capacity = bh1->capacity.bytes + bh2->capacity.bytes + bh3->capacity.bytes;
            if (new_capacity > old_capacity) status = TEST_OK;
            else status = TEST_ERROR;
        } else status = TEST_ERROR;
    } else status = TEST_ERROR;
    fprintf(result, "%s","Heap after growing: \n");
    debug_heap(result, bh1);
    _free(var2);
    _free(var1);
    if (bh1->next == NULL) status = TEST_ERROR;
    fprintf(result, "%s","Heap after releasing: \n");
    debug_heap(result, bh1);
    fprintf(result,"%s%s" , "Test: growing heap without extending", test_msg[status]);
    fprintf(result, "%s","\n");
    return status;
}

enum test_status testing(FILE* test_result) {

    heap_init(8175);
    return allocated_memory_test(test_result) |
    free_one_block_test(test_result) |
    free_two_blocks_test(test_result) |
    grow_heap_with_extending_test(test_result) |
    grow_heap_without_extending_test(test_result);
}
