Test 1: 
Heap after memory allocation: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000         24    taken   0000
 0x4040029       8134     free   0000
Test: allocation memory was successful

Test 2: 
Heap before memory releasing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000         24    taken   0000
 0x4040029         24    taken   A000
 0x4040052         24    taken   0000
 0x404007b       8052     free   0000
Heap after memory releasing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000         24    taken   0000
 0x4040029         24     free   A000
 0x4040052         24    taken   0000
 0x404007b       8052     free   0000
Test: free one block was successful

Test 3: 
Heap before memory releasing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000         24    taken   0000
 0x4040029         24    taken   A000
 0x4040052         24    taken   0000
 0x404007b       8052     free   0000
Heap after memory releasing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000         24    taken   0000
 0x4040029       8134     free   A000
Test: free two blocks was successful

Test 4: 
Heap after growing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000         24    taken   0000
 0x4040029      10134    taken   1000
 0x40427d0      10271     free   0000
Heap after releasing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000      20463     free   0000
Test: growing heap with extending was successful

Test 5: 
Heap after growing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000        100    taken   0000
 0x404140e      17329    taken   1000
 0x40457d0      18463     free   0000
Heap after releasing: 
 --- Heap ---
     start   capacity   status   contents
 0x4040000        100     free   0000
 0x404140e      35809     free   1000
Test: growing heap without extending was successful

